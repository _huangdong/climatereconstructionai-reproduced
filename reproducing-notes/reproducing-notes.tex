%! Author = dh146
%! Date = 16/08/20

% Preamble
\documentclass[11pt]{article}

% Packages
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{minted}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{xcolor}
\usepackage{cleveref}

% Document
\begin{document}

    \title{Notes for Reproducing (Kadow et al. 2020)}
    \author{Dong Huang}

    \maketitle

    \subsection*{Package versions}

    Installing Python packages from \verb|pip install -r requirements.txt| is not sufficient.
    A virtual environment with Python version 3.7 must be created to use the older versions of packages listed in the \verb|requirements.txt|.

    In addition, simply installing \verb|numpy==1.14.4| and \verb|torch==0.4.1| does not work well, from which the \verb|RuntimeError| will be raised saying that ``PyTorch was compiled without NumPy support''.

    A post at \verb|https://github.com/pytorch/pytorch/issues/6747| gives the solution to this.
    \verb|numpy==1.15.0| and then the PyTorch with version \verb|torch==0.4.1.post2| can be installed.

    \paragraph{{\color{blue}Update:}}
    If \verb|xESMF| (0.3.0) is to be installed, the required minimal version of \verb|numpy| is 1.15.4.
    In addition, the version of \verb|scipy| should not exceed 1.4.1, otherwise the following error will occur:
    {\footnotesize\begin{verbatim}
    ModuleNotFoundError: No module named 'numpy.core._multiarray_umath'
    \end{verbatim}}

    In summary, the following versions are installed:
    \begin{itemize}
        \item numpy: 1.15.4
        \item torch: 0.4.1.post2
        \item scipy: 1.4.1
        \item xESMF: 0.3.0
    \end{itemize}

    \subsection*{Gridding}

    According to the paper, the gridding are 5 degrees by 5 degrees in both latitude and longitude.
    However, the mask provided in \verb|masks/hadcrut4-missmask.h5| has the following coordinates:
    \begin{itemize}
        \item lon: $2.5, 7.5, 12.5, 17.5, \cdots, 347.5, 352.5, 357.5$
        \item lat: $-88.75, -86.25, -83.75, \cdots, 83.75, 86.25, 88.75$
    \end{itemize}
    So this is a $2.5^{\circ}\times5^{\circ}$ (lat-by-lon) gridding.

    \subsection*{The 20CR dataset}

    \paragraph{Data variable} Variable ``TMPS'' (surface air temperature) is downloaded, with monthly data from 1806 to 2015.
    
    \paragraph{Resolution} The original resolution is approximately $0.7^{\circ}\times0.7^{\circ}$, and is resampled into $2.5^{\circ}\times5^{\circ}$ (lat-by-lon) as required.
    
    {\color{red} \paragraph{Copyright and license} The section ``Privacy and Security''\footnote{https://www.lbl.gov/disclaimers/} declared by the Lawrence Berkeley National Laboratory of the US, linked from the 20CR-project's page https://portal.nersc.gov/project/20C\_Reanalysis/, does not explicitly state whether and how a license or agreement is required to use the 20CR dataset for nonprofit research.
    However, the web directory at NOAA/PSL\footnote{The URL is https://portal.nersc.gov/archive/home/projects/incite11/www/20C\_Reanalysis\_version\_3} from which I downloaded the TMPS data two weeks ago is no longer available (\Cref{fig:20cr-psl}).
    \textit{Not sure if this is just a technical issue or a change of access control.}

    The 20CR data are hosted from two sources: one is NOAA/PSL described in above, and the other is the NCAR (National Center for Atmospheric Research), located at https://rda.ucar.edu/datasets/ds131.3/.
    The NCAR explicitly requires a register flow to gain the access.

    Overall, it is not sure whether the data downloaded two weeks ago from a public accessible link on PSL's website are still free to be used or not.
    But there are alternatives and the CMIP5 dataset can be also tried.
    }

    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{images/screenshot.png}
        \caption{Error accessing the 20CR dataset currently.}
        \label{fig:20cr-psl}
    \end{figure}


    \subsection*{Computational expense}

    The original code read and parse the entire HDF5 files for both 20CR and mask dataset each time the \verb|__getitem__()| is called, causing huge overhead and a training speed at about 0.2 iteration per second.
    Simply moving the read data to the class member improved the speed to 31 iterations per second.
    A 150x boost.

    \subsection*{Use of the masks}
    In the original repository Kadow et al.~referred to (https://github.com/naoto0804/pytorch-inpainting-with-partial-conv), the mask is randomly picked:
    \begin{minted}[fontsize=\footnotesize]{python}
    # Repository: https://github.com/naoto0804/pytorch-inpainting-with-partial-conv
    # File: places2.py
    ...
    def __getitem__(self, index):
        gt_img = Image.open(self.paths[index])
        gt_img = self.img_transform(gt_img.convert('RGB'))

        mask = Image.open(self.mask_paths[random.randint(0, self.N_mask - 1)])
        mask = self.mask_transform(mask.convert('RGB'))
        return gt_img * mask, mask, gt_img
    ...
    \end{minted}

    However, in Kadow et al.'s modification, the mask for the instance is selected by the exact same index as ``image'':
\begin{minted}[fontsize=\footnotesize]{python}
    # Repository: https://github.com/FREVA-CLINT/climatereconstructionAI
    # File: places2.py
    ...
    def __getitem__(self, index):
        ...
        mask_file = h5py.File(self.maskpath)
        maskdata = mask_file.get('tas')
        N_mask = len((maskdata[:,1,1]))
        mask = torch.from_numpy(maskdata[index,:,:])
        ...
    ...
\end{minted}

    It is unclear why Kadow et al. made such a modification.
    The masks in \verb|masks/hadcrut4-missmask.h5| seem to be organized by time: from the largest masked area at the beginning, to a almost-revealed mask at the end, as illustrated in \Cref{fig:masks}:

    \begin{figure}[tb]
        \centering
        \begin{subfigure}{.48\textwidth}
            \includegraphics[width=\textwidth]{images/mask-1.pdf}
            \caption{index=1}
        \end{subfigure}
        \begin{subfigure}{.48\textwidth}
            \includegraphics[width=\textwidth]{images/mask-600.pdf}
            \caption{index=600}
        \end{subfigure}
        \begin{subfigure}{.48\textwidth}
            \includegraphics[width=\textwidth]{images/mask-1200.pdf}
            \caption{index=1200}
        \end{subfigure}
        \begin{subfigure}{.48\textwidth}
            \includegraphics[width=\textwidth]{images/mask-last.pdf}
            \caption{index=-1}
        \end{subfigure}
        \caption{Masks for different indices.}
        \label{fig:masks}
    \end{figure}

    \paragraph{\color{blue} Update:}
    In (Kadow et al., 2020) it mentioned to use ``random (train) and specific (evaluate) missing-value masks''.
    So the file \verb|hadcrut4-missmask.h5| should be used for evaluation.
    For training, it is still unclear how many random masks are generated.
    Use the default ``10,000'' in the reproducing.


\end{document}