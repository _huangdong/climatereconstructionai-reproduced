import random
from datetime import datetime
import torch
#import torch.multiprocessing as mp
#mp.set_start_method('spawn')
import h5py
import numpy as np

from PIL import Image
from glob import glob
import torch.utils.data as data

class Places2(torch.utils.data.Dataset):
    def __init__(self, img_root, mask_root, img_transform, mask_transform,
                 split='train', image_size=72, n_masks=10000):
        super(Places2, self).__init__()

        self.image_size = image_size
        self.n_masks = n_masks
        
        self.img_transform = img_transform
        self.mask_transform = mask_transform
        # use about 8M images in the challenge dataset
        if split == 'train':
            self.paths = glob('{:s}/data_large/**/*.h5'.format(img_root),
                              recursive=True)
        else:
            self.paths = glob('{:s}/{:s}_large/**/*.h5'.format(img_root, split))

        #self.h5_file = h5py.File('{:s}'.format(self.paths[0]), 'r')
        #self.hdata = self.h5_file.get('tas')
        #self.leng = len((self.hdata[:,1,1]))
        #print(self.leng)
        #print(self.hdata)
        self.maskpath = mask_root
        #self.mask_file = h5py.File(mask_root)
        #self.maskdata = self.mask_file.get('tas')
        #self.N_mask = len((self.maskdata[:,1,1]))

        self._prepare_data()
            
    def __getitem__(self, index):
        gt_img = self.hdata[index]
        mask = self.maskdata[random.randint(0, len(self.maskdata) - 1)]

        # gt_img = self.hdata[index,:,:]
        # # gt_img -= np.mean(gt_img) # the -= means can be read as x = x- np.mean(x)
        # # gt_img /= np.std(gt_img) # the /= means can be read as x = x/np.std(x)
        # gt_img = torch.from_numpy(gt_img[:, :]).float()
        # gt_img = gt_img.unsqueeze(0)
        # a = gt_img[0,:,:]
        # gt_img = a.repeat(3, 1, 1)
        # #gt_img = self.img_transform(gt_img)
        #
        # N_mask = len((self.maskdata[:, 1, 1]))
        # mask = torch.from_numpy(self.maskdata[random.randint(0, N_mask - 1), :, :])
        # mask = mask.float()
        # # mask = torch.from_numpy(self.maskdata[index,:,:])
        # #mask = torch.from_numpy(maskdata[random.randint(0, N_mask - 1),:,:])#.float()
        # mask = mask.unsqueeze(0)
        # b = mask[0,:,:]
        # mask = b.repeat(3, 1, 1)
        #
        # #print(gt_img)
        # #print(mask)
        
        return gt_img * mask, mask, gt_img

    def __len__(self):
        return len(self.hdata[:, 1, 1])

    def _prepare_data(self):
        hdata = []
        for path in self.paths:
            h5_file = h5py.File(path, 'r')
            hdata.append(h5_file.get('tas'))
        hdata = np.vstack(hdata)
        print(f'Length of hdata: {len(hdata)}')


        from generate_data import random_walk_batch
        image_size = self.image_size
        masks = []
        print('Generating masks...',)
        t_begin = datetime.now()
        canvases = np.ones((self.n_masks, image_size, image_size), dtype=np.int)
        masks = random_walk_batch(canvases, image_size ** 2)

        maskdata = np.stack(masks)
        print(f'completed. ({(datetime.now() - t_begin).total_seconds():.1f} seconds)')

        # Additional operations
        def copy_channels(data: np.ndarray):
            print(type(data))
            channel_copier = np.ones((1, 3, 1, 1))
            l, w, h = data.shape
            return data.reshape((l, 1, w, h)) * channel_copier

        hdata = copy_channels(hdata[:, :, :])
        maskdata = copy_channels(maskdata)

        hdata = torch.from_numpy(hdata).float()
        maskdata = torch.from_numpy(maskdata).float()

        self.hdata = hdata
        self.maskdata = maskdata

