import os, sys
import xesmf as xe


class HiddenPrints:
    def __enter__(self):
        self._original_stdout = sys.stdout
        sys.stdout = open(os.devnull, 'w')

    def __exit__(self, exc_type, exc_val, exc_tb):
        sys.stdout.close()
        sys.stdout = self._original_stdout


def regrid(ds_in, target_ds, method='bilinear', periodic=True):
    with HiddenPrints():
        regridder = xe.Regridder(ds_in, target_ds, method, periodic, reuse_weights=True)
    ds_out = regridder(ds_in)

    return ds_out

