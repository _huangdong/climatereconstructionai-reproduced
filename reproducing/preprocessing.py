import gc
import os
from glob import glob
import time
import argparse

import numpy as np
import xarray as xr

from regrid import regrid

MEMBER_FIRST = 1
MEMBER_LAST = 81
LATITUDE_RANGE = (-88.75, 90, 2.5)
LONGITUDE_RANGE = (2.5, 360, 5)


def regrid_hadcrut4(root, out_dir='.', lat_range=LATITUDE_RANGE,
                    lon_range=LONGITUDE_RANGE):
    with xr.open_dataset(os.path.join(root, 'HadCRUT.4.6.0.0.median.nc')) as dset:
        dest_grid = xr.Dataset({
            'lat': ('lat', np.arange(*lat_range)),
            'lon': ('lon', np.arange(*lon_range))
        })

        dset_tmp = dset['temperature_anomaly']
        ess = regrid(dset_tmp, dest_grid)
        dset_out = xr.Dataset({'tas': ess})
        dset_out.to_netcdf(os.path.join(out_dir, f'20cr-mem{member:03}.h5'),
                           engine='h5netcdf')


def regrid_20cr(root, member_first=MEMBER_FIRST, member_last=MEMBER_LAST,
                out_dir='.', lat_range=LATITUDE_RANGE, lon_range=LONGITUDE_RANGE,
                large_mem=False):
    filenames_20cr = os.path.join(root, r'TMPS/*/TMPS.*.mnmean_mem{:03}.nc')
    for member in range(member_first, member_last + 1):
        print('-'*15 + '\n' + f'Opening {filenames_20cr.format(member)}')
        # print(f'Files are {glob(filenames_20cr.format(member))}')

        merged = [] if large_mem else None
        for file_name in glob(filenames_20cr.format(member)):
            with xr.open_dataset(file_name) as dset:
                lats = np.arange(*lat_range)
                lons = np.arange(*lon_range)
                d = {'lat': (('lat'), lats), 'lon': (('lon'), lons)}
                dest_grid = xr.Dataset(d)

                dset_tmp = dset['TMP']
                ess = regrid(dset_tmp, dest_grid)
                if large_mem:
                    merged.append(ess)
                else:
                    if merged is None:
                        merged = xr.Dataset({'tas': ess})
                    else:
                        merged = xr.merge((merged, xr.Dataset({'tas': ess})))
                # time.sleep(0.5)

        if large_mem:
            merged = xr.merge(merged).rename({'TMP': 'tas'})
        print("Merged dataset: ", merged)

        # Compute the anomalies
        climatology = merged.where(
                (merged.time >= np.datetime64('1961-01-01')) &
                (merged.time <= np.datetime64('1990-12-31')), drop=True
                ).groupby('time.month').mean('time')
        anomalies = merged.groupby('time.month') - climatology
        print("Anomaly dataset: ", anomalies)
        anomalies.to_netcdf(os.path.join(out_dir, f'20cr-mem{member:03}.h5'),
                            engine='h5netcdf')
        gc.collect()


def argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--root', type=str, default='/home/dh146/Downloads/20CR')
    parser.add_argument('--out_dir', type=str, default='.')
    parser.add_argument('--member_first', type=int, default=MEMBER_FIRST)
    parser.add_argument('--member_last', type=int, default=MEMBER_LAST)
    parser.add_argument('--lat', type=list, nargs=3, default=list(LATITUDE_RANGE))
    parser.add_argument('--lon', type=list, nargs=3, default=list(LONGITUDE_RANGE))

    return parser


def main():
    parser = argument_parser()
    args = parser.parse_args()
    regrid_20cr(args.root, member_first=args.member_first, member_last=args.member_last,
                out_dir=args.out_dir, lat_range=args.lat, lon_range=args.lon)


if __name__ == '__main__':
    main()
